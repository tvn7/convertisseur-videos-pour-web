param (
    [string]$inputVideo
)

# Vérifier si l'argument $inputVideo est fourni
if (-not $inputVideo) {
    Write-Host "Usage: .\nom_du_script.ps1 -inputVideo <chemin_vers_votre_video.mp4>"
    exit
}

$outputDirectory = ".\output\"
$ffmpegPath = "ffmpeg"
$baseName = [System.IO.Path]::GetFileNameWithoutExtension($inputVideo)

# Convertir la vidéo en H265
Write-Host "Conversion H265..."
$outputH265 = Join-Path $outputDirectory $baseName"_h265.mp4"
& $ffmpegPath -i $inputVideo -c:v libx265 -crf 28 -preset fast -c:a aac -b:a 128k -v quiet -stats -x265-params log-level=error $outputH265 

# Convertir la vidéo en H264 MP4
Write-Host "Conversion H264..."
$outputH264 = Join-Path $outputDirectory $baseName"_h264.mp4"
& $ffmpegPath -i $inputVideo -c:v libx264 -crf 23 -preset fast -c:a aac -b:a 128k -v quiet -stats $outputH264

# Créer les versions en différentes résolutions et les encoder en VP9 avec l'extension .webm
$resolutions = @{
    "360"  = "scale=640:360"
    "720"  = "scale=1280:720"
    "1080" = "scale=1920:1080"
}

foreach ($resolution in $resolutions.Keys) {
    $outputFile = Join-Path $outputDirectory $baseName"_"$resolution".webm"
    Write-Host "Conversion $resolution..."
    $scale = $resolutions[$resolution]
    $ffmpegArgs = @("-i", $inputVideo, "-vf", $scale, "-c:v", "libvpx-vp9", "-crf", "30", "-b:v", "0", "-c:a", "libopus", "-b:a", "128k")
    & $ffmpegPath $ffmpegArgs -v quiet -stats $outputFile
}

Write-Output "Conversion terminee."