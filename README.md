# Convertisseur vidéos pour web

## Que fait le script ?

Ce script permet de convertir des vidéos dans les formats suivants :

- webm 1080p encodé en VP9
- webm 720p encodé en VP9
- webm 360p encodé en VP9
- mp4 1080p encodé en H.264
- mp4 1080p encodé en H.265

## Pourquoi faire ?

Pour proposer plusieurs formats et qualité pour un upload web des vidéos. (Notamment pour les interagros).

## Comment ça marche ?

Placer toutes les vidéos au format mp4 dans le dossier avec les scripts ps1.

Lancer la commande `./iterate.ps1 fichier` depuis powershell. `fichier` peut être un paramètre par exemple `*.mp4`

Le script va tourner et les vidéos converties seront placées dans le dossier `output`
