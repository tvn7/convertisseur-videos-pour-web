param (
    [string]$filter
)

# Vérifier si l'argument $inputVideo est fourni
if (-not $filter) {
    Write-Host "Usage: .\nom_du_script.ps1 -inputVideo <chemin_vers_votre_video.mp4>"
    exit
}

# Obtenir tous les fichiers .mp4 dans le dossier
$videos = Get-ChildItem -Path $folderPath -Filter $filter

# Exécuter le script convert.ps1 pour chaque fichier vidéo
foreach ($video in $videos) {
    # Affiche le fichier en cours de traitement
    Write-Host "Traitement de la video : $($video.FullName)"
    
    # Exécute le script avec le fichier vidéo comme argument
    .\convert.ps1 $video.FullName
}